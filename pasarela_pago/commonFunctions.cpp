#include"commonFunctions.h"
#include<qmath.h>
quint8 getXor(QByteArray mensaje,int len)
{
    quint8 resultado;
    resultado=mensaje[1];
    for(int i=2;i<len;i++)
    {
        resultado=resultado^mensaje[i];
    }

    return resultado;
}
QByteArray intToBits(quint8 entero)
{
  //  entero=11;
   QByteArray salida;
   int ini=entero;
   while(ini>0)
   {
       int ret=ini%2;
       salida.prepend(quint8(ret));
       ini=ini/2;
   }
   for(int i=salida.count();i<8;i++)
   {
       quint8 cero=0x00;
       salida.prepend(cero);
   }


   return salida;
}
int bitToInt(QList<bool> bits)
{
    int salida=0;
    for(int i=0;i<bits.count();i++)
    {
        int exponente= bits.count()-1-i;
        int val=bits[i];
        salida+=val*(qPow(2,exponente));
    }
    return salida;
}
WORD CalcularCRC(QByteArray Buffer, BYTE len)
{
    WORD crc;	// Acumulador CRC
    BYTE i,j;	// Contadores

    crc = 0;
    for(j = 0; j < len; j++)
    {
        crc = crc ^ (Buffer[j] << 8);
        for(i = 0; i < 8; i++)
        {
            if ((crc & 0x8000) == 0x8000)
            {
                crc = crc << 1;
                crc ^= 0x1021;
            }
            else
            {
                crc = crc << 1;
            }
        }
    }
    return(crc);
}
