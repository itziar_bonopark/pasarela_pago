#include "servidor.h"

Servidor::Servidor(QObject *parent) :
    QTcpServer(parent)
{
    connect(this,SIGNAL(newConnection()),this,SLOT(sltNuevaConexion()));
}

void Servidor::sltNuevaConexion()
{
    QTcpSocket* cliente = nextPendingConnection();
    connect(cliente,SIGNAL(disconnected()),this,SLOT(clienteMuerto()));
    connect(cliente,SIGNAL(readyRead()),this,SLOT(sltLectura()));
    connect(cliente,SIGNAL(destroyed()),this,SLOT(sltDestruccion()));
    this->listaDeClientes.append(cliente);
}

void Servidor::clienteMuerto()
{
    QTcpSocket* cliente=qobject_cast<QTcpSocket*>(sender());
    if(!cliente)
        return;
    this->listaDeClientes.removeAll(cliente);
    cliente->deleteLater();
    //qDebug()<<"ya tio...";
}

void Servidor::sltLectura()
{
    QTcpSocket* cliente=qobject_cast<QTcpSocket*>(sender());

    QByteArray mensaje=cliente->readAll();
    emit this->sgnMensajeConCliente(mensaje,cliente);
    if((cliente->peerPort()==9896)||(cliente->localPort()==9896))
    {

    }
    else
    {
        cliente->close();
    }


}
void Servidor::sltDestruccion()
{
    //qDebug()<<"Destruccion";
}
