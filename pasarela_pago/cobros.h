#ifndef COBROS_H
#define COBROS_H

#include <TpvpcLatente.h>
#include <QDebug>
#include <QXmlStreamReader>
#include <QFile>
#include <QtXml>
#include <cimpresora.h>
#include <QDateTime>
#include <QSettings>
#include <QTcpSocket>
#include <servidor.h>


class cobros : public QObject
{
    Q_OBJECT
public:
    explicit cobros(QObject *parent = 0);
    ~cobros();

private slots:
    void iniciar_comunicacion();

    QString preautorizacion(QString importe);

    QString leer_xml(QDomDocument xmlBOM);

    QString anulacion(QString pedido,QString rts);

    void imprimirTicket(QString texto);

    void imprimirTicketDenegada();
    void imprimirTicketAnulacion();

    void imprimir_preautorizacion();

    void imprimir_anulacion();
    void obtenerConfiguracion();

    void sltDatosDesdeInterfaz(QByteArray msg, QTcpSocket *sk);

    QString recarga(QString importe);
    QString anulacionRecarga(QString pedido,QString rts,QString importe);
    void imprimir_recarga();
    void imprimirTicketAnulacionRecarga();


private:
    QDomDocument xmlBOM;
    CImpresora* impresora;
    QString puerto_pagos;
    QString numero_comercio;
    QString terminal;
    QString clave_comercio;
    QString version_comercio;
    QSettings* mSettings;
    Servidor *srv;
};

#endif // COBROS_H
