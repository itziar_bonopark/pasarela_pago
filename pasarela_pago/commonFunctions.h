#ifndef COMMONFUNCTIONS_H
#define COMMONFUNCTIONS_H
#include<QObject>


#define BYTE unsigned char
#define WORD unsigned int
#define DWORD unsigned long

// Sólo para Lectura
#define L(v)  ((BYTE) (v))
#define H(v)  ((BYTE) (((WORD) (v)) >> 8))

// Para Lectura y Escritura
#define LOW(v)   (*(((BYTE *) (&v) + 1)))
#define HIGH(v)  (*((BYTE *) (&v)))


quint8 getXor(QByteArray mensaje,int len);
QByteArray intToBits(quint8 entero);
int bitToInt(QList<bool> bits);
WORD CalcularCRC(QByteArray Buffer, BYTE len);
#endif // COMMONFUNCTIONS_H
