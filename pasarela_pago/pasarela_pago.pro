#-------------------------------------------------
#
# Project created by QtCreator 2014-11-24T12:37:24
#
#-------------------------------------------------

QT       += core xml network

QT       -= gui

TARGET = pasarela_pago
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    cimpresora.cpp \
    servidor.cpp \
    commonFunctions.cpp \
    cobros.cpp

HEADERS += \
    TpvpcLatente.h \
    servidor.h \
    commonFunctions.h \
    cimpresora.h \
    cobros.h

OTHER_FILES += \
    anulacion.xml

unix|win32: LIBS += -lImplantadoLinux
CONFIG += extserialport
LIBS+= -L/usr/local/lib -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_flann


