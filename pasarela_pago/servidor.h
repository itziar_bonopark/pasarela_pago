#ifndef SERVIDOR_H
#define SERVIDOR_H
#include<QTcpSocket>
#include <QTcpServer>



class Servidor : public QTcpServer
{
    Q_OBJECT
public:
    explicit Servidor(QObject *parent = 0);


signals:
    void sgnMensajeConCliente(QByteArray msg,QTcpSocket* sk);
    
public slots:
    void sltNuevaConexion();
    void clienteMuerto();
    void sltLectura();
    void sltDestruccion();


private:
    QList<QTcpSocket*> listaDeClientes;
    QList<QByteArray> listaMensajesDesdeTotem;
};

#endif // SERVIDOR_H
