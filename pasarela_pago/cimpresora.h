#ifndef CIMPRESORA_H
#define CIMPRESORA_H

#include <QObject>
#include <qextserialport.h>
#include<QDebug>
#include<opencv/cv.h>
#include<opencv/highgui.h>
#include <QFile>
#include<QTextStream>
#include"commonFunctions.h"

const float PRECIO_BASE_SEMANAL = 10;
const float PRECIO_CLUB_SEMANAL = 5;
const float PRECIO_SEGURO_SEMANAL = 5;
const float PRECIO_CASCO_SEMANAL = 10;
const float PRECIO_30_GRATIS_SEMANAL = 2;
const float PRECIO_TARIFA_PLANA_SEMANAL = 15;
const float PRECIO_USOS_ILIMITADOS_SEMANAL = 3;


const float PRECIO_BASE_DIARIO = 5;
const float PRECIO_CLUB_DIARIO = 3;
const float PRECIO_SEGURO_DIARIO = 2;
const float PRECIO_CASCO_DIARIO = 10;
const float PRECIO_30_GRATIS_DIARIO = 1;
const float PRECIO_TARIFA_PLANA_DIARIO = 15;
const float PRECIO_USOS_ILIMITADOS_DIARIO = 1;


const int JUSTIFICACION_IZQUIERDA = 0;
const int JUSTIFICACION_CENTRADO = 1;
const int JUSTIFICACION_DERECHA = 2;

const int COMANDO_ESTADO = 0x27;
const int COMANDO_IMPRIMIR = 0x29;

class CImpresora : public QObject
{
    Q_OBJECT
public:
    explicit CImpresora(QObject *parent = 0);
    void imprimirTexto(QString texto, bool fin=true, int salto=20);
    void setJustificacion(int jus);
    void setCharacterSize(int siz);
    void setCharacterFont(int v=0);
    void abrirPuerto();
    void selectBitImageMode(int m, int nL, int nH,QList<int> d);
    void imprimirLogo(QString imagen);
    void getEstado();

    bool getencendido(){ return this->bencendido;}
    bool getpapelsuficiente(){ return this->bpapelsuficiente;}

    bool getpapelpresente(){ return this->bpapelpresente;}
    bool gethaypapel(){ return this->bhaypapel;}
    bool getok(){return this->ok;}


    QextSerialPort *port;
    QByteArray estadoImpresora;
    QByteArray bufferRecepcion;
    QString puerto_impresora;
    QString ruta_logo_bicimad;
    QString ruta_logo_contacless;

signals:
    void estadoRecibido();

    void enviadoALaImpresora(QByteArray);
    void recibidoDeLaImpresora(QByteArray);

public slots:
    void recepcion();
private:

    int iLastCommand;
    bool bencendido;
    bool bcutterOn;
    bool bpapelsuficiente;
    bool bPapelApuntoDeAcabar;
    bool bpapelpresente;
    bool bhaypapel;
    bool ok;
};

#endif // CIMPRESORA_H
