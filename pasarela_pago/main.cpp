#include <QtCore/QCoreApplication>
#include<QFile>
#include<QDir>
#include<QtGlobal>
#include <syslog.h>
#include <QTextCodec>
#include<QDebug>
#include <cobros.h>


void myMessageOutput(QtMsgType type, const char *msg)
{
    //in this function, you can write the message to any stream!
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s\n", msg);
        syslog (LOG_NOTICE, "%s",msg);

        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s\n", msg);
        syslog (LOG_WARNING,"%s", msg);

        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s\n", msg);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s\n", msg);
        abort();
    }
}



int main(int argc, char *argv[])
{
    setlogmask (LOG_UPTO (LOG_NOTICE|LOG_WARNING|LOG_CRIT));
    openlog ("pagos totem", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
    qInstallMsgHandler(myMessageOutput); //install : set the callback

    QCoreApplication a(argc, argv);
    QTextCodec *linuxCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(linuxCodec);
    QTextCodec::setCodecForCStrings(linuxCodec);
    QTextCodec::setCodecForLocale(linuxCodec);

    cobros* abonos=new cobros;
    return a.exec();
}
