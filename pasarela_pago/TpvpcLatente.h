/*
 * TpvpcLatente.h
 */

#ifndef TPVPCLATENTE_H_
#define TPVPCLATENTE_H_

/* Función que inicia la aplicación TPV-PC LATENTE */
extern "C" int fnDllIniTpvpcLatente(char * cComercio, char * cTerminal, char * cClaveFirma, char * cConfPuerto, char * cVersion);


/* Fuerza la parada de TPV-PC Latente. No devuelve errores. */
extern "C" int fnDllParaTpvpcLatente();


/* Función que realiza una petición de Pago o Preautorización al TPV-PC utilizando un dispositivo PINPAD */
extern "C" int fnDllOperPinPad(char * cImporte, char * cFactura, char * cTipoOper, char * cXMLResp, int iTamMaxResp);


/* Función que confirma como se quiere realizar una operación que es DCC.
 * Este método sólo debe ser llamado después de fnDllOperPinPad */
extern "C" int fnDllOperPinPadDCC(char * cImporte, char * cFactura, char * cTipoOper, char * cCodDivisa, char * cXmlResp, int iTamMaxResp);


/* Función que confirma como se quiere realizar una operación que es DCC.
 * Este método sólo debe ser llamado después de fnDllOperManualExt */
extern "C" int fnDllOperManualDCC(char * cCodDivisa, char * cXMLResp, int iTamMaxResp);


/* Función que realiza una petición de Pago o Preautorización al TPV-PC tecleando
 * manualmente la tarjeta, caducidad y cvc2 */
extern "C" int fnDllOperManualExt(char * cTarjeta, char * cCaducidad, char *  cCVC2, char * cImporte, char * cFactura, char * cTipoOper, char * cXMLResp, int iTamMaxResp);


/* Función que realizará una petición de Devolución/Confirmación de una operación realizada en el TPV-PC */
extern "C" int fnDllOperComContable(char * cNumPedido, char * cRTSOriginal, char * cImporte, char * cFactura, char * cTipoOper, char * cXMLResp, int iTamMaxResp);

/* Función que realizará una petición de Devolución/Confirmación de una operación realizada en el TPV-PC */
extern "C" int fnDllOperComContableTerminal(char * cNumTerminal, char * cNumPedido, char * cRTSOriginal, char * cImporte, char * cFactura, char * cTipoOper, char * cXMLResp, int iTamMaxResp);


/*	Función que realizará una petición de Devolución Sin Original */
extern "C" int fnDllOperDevSinOrig(char * cTarjeta, char * cCaducidad, char * cImporte, char * cFactura, char * cXMLResp, int iTamMaxResp);

/* Función para realizar consulta de operaciones realizadas con el
 * comercio terminal especificado en la función de Inicialización */
extern "C" int fnDllOperConsulta(char * cNumPedido, char * cRTS, char * cFactura, char * cFechaIni, char * cFechaFin, char * cTipo, char * cResultado, char * cNumPagina, char * cXMLResp, int iTamMaxResp);


/* Función que realiza una consulta de Totales, sobre el comercio/terminal especificado al iniciar la librería */
extern "C" int fnDllOperTotales(char * cFecha, char * cDesgloseVM, char * cDesgloseMarcas, char * cXMLResp, int iTamMaxResp);


/* Función que realizará una Anulación de una operación de Preautorización realizada en el TPV-PC */
extern "C" int fnDllOperAnulPreaut(char * cNumPedido, char * cRTSOriginal, char * cFactura, char * cXMLResp, int iTamMaxResp);


/* Función que realizará un Reemplazo de una operación de Preautorización realizada en el TPV-PC */
extern "C" int fnDllOperReempPreaut(char * cNumPedido, char * cRTSOriginal, char * cImporte, char * cFactura, char * cXMLResp, int iTamMaxResp);


/* Función que realizará una repetición de una operación de pago con tarjeta en archivo realizada en el TPV-PC */
extern "C" int fnDllOperPagoTjtArchivo(char * cNumPedido, char * cRTSOriginal, char * cFactura, char * cXMLResp, int iTamMaxResp);


/* Método que continua una operación de la que se han devuelto Opciones de Pago */
extern "C" int fnDLLTrataOpcionesPago(char * cOpcionPago, char * cXMLResp, int iTamMaxResp);


#endif /* TPVPCLATENTE_H_ */
