#include "cobros.h"

cobros::cobros(QObject *parent) :
    QObject(parent)
{
    this->srv=new Servidor;
    connect(this->srv,SIGNAL(sgnMensajeConCliente(QByteArray,QTcpSocket*)),this,SLOT(sltDatosDesdeInterfaz(QByteArray,QTcpSocket*)));
    this->srv->listen(QHostAddress::Any,88888);

    this->impresora= new CImpresora;
    this->obtenerConfiguracion();
}

cobros::~cobros()
{
    fnDllParaTpvpcLatente();
}

void cobros::sltDatosDesdeInterfaz(QByteArray msg, QTcpSocket *sk)
{

    QString mensaje_texto=msg.constData();
    QList<QString> operaciones=mensaje_texto.split("#");
    if(operaciones[0]=="1")//Preautorizacion
    {
        QString resultado= this->preautorizacion(operaciones[1]);
        sk->write(resultado.toAscii());
        if(sk->waitForBytesWritten(500))
        {
            //qDebug()<<"Mensaje escrito!";
        }
        else
        {
            //qDebug()<<"No ha podido escribir";
        }
    }
    if(operaciones[0]=="2")//Anulacion preautorizacion
    {
        QString resultado=this->anulacion(operaciones[1],operaciones[2]);
        sk->write(resultado.toAscii());
        if(sk->waitForBytesWritten(500))
        {
            //qDebug()<<"Mensaje escrito!";
        }
        else
        {
            //qDebug()<<"No ha podido escribir";
        }
    }
    if(operaciones[0]=="4")//Recarga
    {
        QString resultado=this->recarga(operaciones[1]);
        sk->write(resultado.toAscii());
        if(sk->waitForBytesWritten(500))
        {
            //qDebug()<<"Mensaje escrito!";
        }
        else
        {
            //qDebug()<<"No ha podido escribir";
        }
    }
    if(operaciones[0]=="5")//Anulacion recarga
    {
        QString resultado=this->anulacionRecarga(operaciones[1],operaciones[2],operaciones[3]);
        sk->write(resultado.toAscii());
        if(sk->waitForBytesWritten(500))
        {
            //qDebug()<<"Mensaje escrito!";
        }
        else
        {
            //qDebug()<<"No ha podido escribir";
        }
    }
    if(operaciones[0]=="6")//imprimir ticket preautorizacion
    {
        this->imprimir_preautorizacion();
    }
    if(operaciones[0]=="7")//imprimir ticket preautorizacion denegada
    {
        this->imprimirTicketDenegada();
    }
    if(operaciones[0]=="8")//imprimir ticket anulacion preautorizacion
    {
        this->imprimirTicketAnulacion();
    }
    if(operaciones[0]=="9")//imprimir ticket recarga
    {
        this->imprimir_recarga();
    }
    if(operaciones[0]=="10")//imprimir ticket recarga denegada
    {
        this->imprimirTicketDenegada();
    }
    if(operaciones[0]=="11")//imprimir ticket devolucion recarga
    {
        this->imprimirTicketAnulacionRecarga();
    }
}

void cobros::obtenerConfiguracion()
{
    QString m_sSettingsFile="../configuraciones/settings.ini";
    this->mSettings=new QSettings(m_sSettingsFile, QSettings::NativeFormat);

    this->impresora->puerto_impresora=this->mSettings->value("puerto_impresora","/dev/ttyS3").toString();
    this->impresora->ruta_logo_bicimad=this->mSettings->value("ruta_logo").toString();
    this->impresora->ruta_logo_contacless=this->mSettings->value("ruta_logo_contacless").toString();
    this->impresora->abrirPuerto();

    this->puerto_pagos=this->mSettings->value("puerto_pagos","/dev/ttyS0").toString();
    this->numero_comercio=this->mSettings->value("numero_comercio_pinpad").toString();
    this->terminal=this->mSettings->value("terminal_pinpad").toString();
    this->clave_comercio=this->mSettings->value("clave_comercio_pinpad").toString();
    this->version_comercio=this->mSettings->value("version_software","5.1").toString();
    this->iniciar_comunicacion();

}
void cobros::iniciar_comunicacion()
{
    int shStatus = -1;
    QString conexion=this->puerto_pagos+",19200,N,8,1";
    QByteArray ba = conexion.toLatin1();
    char *c_str = ba.data();

    QByteArray ba2 = this->numero_comercio.toLatin1();
    char *c_str2 = ba2.data();

    QByteArray ba3 = this->terminal.toLatin1();
    char *c_str3 = ba3.data();

    QByteArray ba4 = this->clave_comercio.toLatin1();
    char *c_str4 = ba4.data();

    QByteArray ba5 = this->version_comercio.toLatin1();
    char *c_str5 = ba5.data();

    shStatus = fnDllIniTpvpcLatente(c_str2,c_str3,c_str4,c_str,c_str5);

    qDebug()<< shStatus;
    if(shStatus==0)
    {
        qDebug()<<"Comunciaciones inicializada";
    }
    else
    {
        qDebug()<<"Error al inicializar pinpad";
        //exit(1);
    }
}


QString cobros::preautorizacion(QString importe)
{
    QString operacion;

    short shStatus = -1;
    char szImporte[256];

    char szBufferXML[2048];

    QByteArray array = importe.toLocal8Bit();
    char* buffer = array.data();
    memcpy(szImporte,buffer,sizeof(szImporte));

    shStatus = fnDllOperPinPad(szImporte,"Ticket Factura","PREAUTORIZACION",szBufferXML,sizeof(szBufferXML)-1);


    if(shStatus==0)
    {
        qDebug()<<"Operacion realizada correctamente. Hay que comprobar la transaccion";
        qDebug()<<szBufferXML;
        QFile f("preautorizacion.xml");

        if (!f.open(QIODevice::WriteOnly))
        {
            // Error while loading file
            qDebug()<< "Error while loading file";
            exit(1);
        }
        f.write(QByteArray(szBufferXML, sizeof(szBufferXML)));
        f.close();

        if (!f.open(QIODevice::ReadOnly ))
        {
            // Error while loading file
            qDebug()<< "Error while loading file";
            exit(1);
        }
         // Set data into the QDomDocument before processing
        xmlBOM.setContent(&f);
        f.close();

       // xmlBOM.setContent(szBufferXML);
        QString respuesta=leer_xml(xmlBOM);

        QList<QString>resultado=respuesta.split("//");
        if(resultado[0]=="0")
        {
            qDebug()<<"operacion denegada";
            operacion=QString::number(shStatus)+"//"+respuesta;
        }
        else
        {
            qDebug()<< "operacion correcta";
            operacion=QString::number(shStatus)+"//"+respuesta;
        }

    }
    else
    {
        qDebug()<<"Error: "+QString::number(shStatus);
        operacion=QString::number(shStatus)+"//";
    }



    return operacion;

}


void cobros::imprimirTicket(QString respuesta)
{
    QString operacion;
    QList<QString>resultado2=respuesta.split("//");

    QList<QString>resultado=resultado2[1].split("#");

    this->impresora->imprimirLogo("bicimad");

    if(resultado[15]=="TRUE")
    {
        this->impresora->imprimirLogo("contactless");
    }

    if(resultado[16]=="PAGO")
    {
        operacion="VENTA";
    }
    if(resultado[16]=="PREAUTORIZACION")
    {
        operacion="PREAUTORIZACION";
    }

    QList<QString>fecha_operacion=resultado[8].split(" ");
    QList<QString>fecha=fecha_operacion[0].split("-");
    QString fecha_ticket=fecha[2]+"-"+fecha[1]+"-"+fecha[0];
    QList<QString>hora=fecha_operacion[1].split(".");
    QString hora_ticket=hora[0];

    QString texto="BiciMAD.\n";
    texto.append("COMERCIO: "+resultado[1]+"\nTerminal:  "+resultado[2]+"\n\n");
    texto.append("      "+resultado[3]+"\n\n");
    texto.append(resultado[4]+"\nCad: "+resultado[5]+"\n\n");
    texto.append(operacion+"\n\n");
    texto.append("Aut."+resultado[6]+"   CRED   Pedido: "+resultado[7]+"\n");
    texto.append("Fecha: "+fecha_ticket+"   Hora: "+hora_ticket+"\n\n");

    if(resultado[17]=="true")
    {
        texto.append("          "+resultado[12]+"\n");
        texto.append(QString::fromUtf8("Aplicacion: ")+resultado[11]+"\n");
        texto.append(QString::fromUtf8("N. Trans:   ")+resultado[10]+"\n");
        texto.append(QString::fromUtf8("Resp:       ")+resultado[13]+"\n");
        texto.append(QString::fromUtf8("TVR:        ")+resultado[14]+"\n\n");
    }

    texto.append("          "+resultado[0]+" EUR");

    this->impresora->imprimirTexto(texto);
}



QString cobros::leer_xml(QDomDocument xmlBOM)
{
    QString tipoPago;
    QString importe;
    QString comercio;
    QString terminal;
    QString tarjeta;
    QString titular;
    QString caducidad;
    QString codigo_respuesta;
    QString pedido;
    QString fecha_operacion;
    QString estado;
    QString resultado;
    QString identificador_rts;
    QString codigo;
    QString mensaje;
    QString conttrans;
    QString idapp;
    QString etiquetaApp;
    QString codrespauto;
    QString resverificacion;
    QString contactless;
    QString emv;

    // Extract the root markup
    QDomElement root=xmlBOM.documentElement();

    // Get root names and attributes
    QString Type=root.tagName();
    QString version=root.attribute("version","No name");

    // Get the first child of the root
    QDomElement resultadoOperacion=root.firstChild().toElement();

    // Loop while there is a child
    while(!resultadoOperacion.isNull())
    {
        // Check if the child tag name is resultadoOperacion
        if (resultadoOperacion.tagName()=="resultadoOperacion")
        {
            // Get the first child of the component
            QDomElement Child=resultadoOperacion.firstChild().toElement();

            // Read each child of the component node
            while (!Child.isNull())
            {
                // Read Name and value
                if (Child.tagName()=="tipoPago") tipoPago=Child.firstChild().toText().data();
                if (Child.tagName()=="importe") importe=Child.firstChild().toText().data();
                if (Child.tagName()=="comercio") comercio=Child.firstChild().toText().data();
                if (Child.tagName()=="terminal") terminal=Child.firstChild().toText().data();
                if (Child.tagName()=="tarjetaClienteRecibo") tarjeta=Child.firstChild().toText().data();
                if (Child.tagName()=="titularTarjeta") titular=Child.firstChild().toText().data();
                if (Child.tagName()=="caducidad") caducidad=Child.firstChild().toText().data();
                if (Child.tagName()=="codigoRespuesta") codigo_respuesta=Child.firstChild().toText().data();
                if (Child.tagName()=="pedido") pedido=Child.firstChild().toText().data();
                if (Child.tagName()=="fechaOperacion") fecha_operacion=Child.firstChild().toText().data();
                if (Child.tagName()=="estado") estado=Child.firstChild().toText().data();
                if (Child.tagName()=="resultado") resultado=Child.firstChild().toText().data();
                if (Child.tagName()=="identificadorRTS") identificador_rts=Child.firstChild().toText().data();
                if (Child.tagName()=="conttrans") conttrans=Child.firstChild().toText().data();
                if (Child.tagName()=="idapp") idapp=Child.firstChild().toText().data();
                if (Child.tagName()=="etiquetaApp") etiquetaApp=Child.firstChild().toText().data();
                if (Child.tagName()=="codrespauto") codrespauto=Child.firstChild().toText().data();
                if (Child.tagName()=="resverificacion") resverificacion=Child.firstChild().toText().data();
                if (Child.tagName()=="operContactLess") contactless=Child.firstChild().toText().data();
                if (Child.tagName()=="operacionemv") emv=Child.firstChild().toText().data();

                // Next child
                Child = Child.nextSibling().toElement();
            }

        }
        if (resultadoOperacion.tagName()=="Error")
        {
            // Get the first child of the component
            QDomElement Child=resultadoOperacion.firstChild().toElement();

            // Read each child of the component node
            while (!Child.isNull())
            {
                // Read Name and value
                if (Child.tagName()=="codigo") codigo=Child.firstChild().toText().data();
                if (Child.tagName()=="mensaje") mensaje=Child.firstChild().toText().data();

                // Next child
                Child = Child.nextSibling().toElement();
            }
        }
        // Next component
        resultadoOperacion = resultadoOperacion.nextSibling().toElement();
    }

    QString respuesta;
    bool operacion_correcta;

    if((estado=="F") && (resultado=="Autorizada"))
    {
        operacion_correcta=1;
        respuesta=QString::number(operacion_correcta)+"//"+importe+"#"+comercio+"#"+terminal+"#"+tarjeta+"#"+titular+"#"+caducidad+"#"+codigo_respuesta+"#"+pedido+"#"+fecha_operacion+"#"+identificador_rts+"#"+conttrans+"#"+idapp+"#"+etiquetaApp+"#"+codrespauto+"#"+resverificacion+"#"+contactless+"#"+tipoPago+"#"+emv;
    }
    else if(codigo!=NULL)
    {
        operacion_correcta=0;
        respuesta=QString::number(operacion_correcta)+"//"+codigo+"//"+mensaje;
    }
    else if(estado!="F")
    {
        operacion_correcta=0;
        respuesta=QString::number(operacion_correcta)+"//"+estado;
    }
    else
    {
        operacion_correcta=0;
        respuesta=QString::number(operacion_correcta)+"//"+codigo_respuesta;
    }

    return respuesta;
}



QString cobros::anulacion(QString pedido,QString rts)
{
    QString operacion;

    short shStatus = -1;
    char szBufferXML[2048];

    char szNumPedido[256];
    char szRTSOriginal[256];


    QByteArray array2 = pedido.toLocal8Bit();
    char* buffer2 = array2.data();
    memcpy(szNumPedido,buffer2,sizeof(szNumPedido));

    QByteArray array3 = rts.toLocal8Bit();
    char* buffer3 = array3.data();
    memcpy(szRTSOriginal,buffer3,sizeof(szRTSOriginal));

    shStatus = fnDllOperAnulPreaut(szNumPedido, szRTSOriginal, "Ticket Factura", szBufferXML,sizeof(szBufferXML)-1);

    if(shStatus==0)
    {
        qDebug()<<"Operacion realizada correctamente. Hay que comprobar la transaccion";
        qDebug()<<szBufferXML;

        QFile f("anulacion.xml");

        if (!f.open(QIODevice::WriteOnly))
        {
            // Error while loading file
            qDebug()<< "Error while loading file";
            exit(1);
        }
        f.write(QByteArray(szBufferXML, sizeof(szBufferXML)));
        f.close();

        if (!f.open(QIODevice::ReadOnly ))
        {
            // Error while loading file
            qDebug()<< "Error while loading file";
            exit(1);
        }
         // Set data into the QDomDocument before processing
        xmlBOM.setContent(&f);
        f.close();

       // xmlBOM.setContent(szBufferXML);
        QString respuesta=this->leer_xml(xmlBOM);

        QList<QString>resultado=respuesta.split("//");

        if(resultado[0]=="0")
        {
            qDebug()<<"operacion denegada";
            operacion=QString::number(shStatus)+"//"+respuesta;
        }
        else
        {
            qDebug()<< "operacion correcta";
            operacion=QString::number(shStatus)+"//"+respuesta;
        }

    }
    else
    {
        qDebug()<<"Error: "+QString::number(shStatus);
        operacion=QString::number(shStatus)+"//";
    }

    return operacion;

}


void cobros::imprimirTicketDenegada()
{
    this->impresora->imprimirLogo("bicimad");

    QString texto="BiciMAD.\n";
    texto.append("Fecha: "+QDateTime::currentDateTime().toString("dd/MM/yyyy")+"   Hora: "+QTime::currentTime().toString("hh:mm:ss")+"\n\n");
    texto.append(QString::fromUtf8("Su operacion bancaria ha sido denegada.\n\n"));
    texto.append("             OPERACION DENEGADA\n\n");

    this->impresora->imprimirTexto(texto);
}

void cobros::imprimirTicketAnulacion()
{
    this->impresora->imprimirLogo("bicimad");

    QString texto="BiciMAD.\n";
    texto.append("Fecha: "+QDateTime::currentDateTime().toString("dd/MM/yyyy")+"   Hora: "+QTime::currentTime().toString("hh:mm:ss")+"\n\n");
    texto.append(QString::fromUtf8("No se ha podido comprar el abono por motivos tecnicos.\nDisculpen las molestias.\n\n"));
    texto.append("             OPERACION ANULADA\n\n");

    this->impresora->imprimirTexto(texto);
}

void cobros::imprimir_preautorizacion()
{
    QFile f("preautorizacion.xml");

    if (!f.open(QIODevice::ReadOnly ))
    {
        // Error while loading file
        qDebug()<< "Error while loading file";
        exit(1);
    }
     // Set data into the QDomDocument before processing
    xmlBOM.setContent(&f);
    f.close();

   // xmlBOM.setContent(szBufferXML);
    QString respuesta=this->leer_xml(xmlBOM);

    QList<QString>resultado=respuesta.split("//");

    if(resultado[0]=="0")
    {
        qDebug()<<"operacion denegada";
        this->imprimirTicketDenegada();
    }
    else
    {
        qDebug()<< "operacion correcta";
        this->imprimirTicket(respuesta);
    }
}

void cobros::imprimir_anulacion()
{
    QFile f("anulacion.xml");

    if (!f.open(QIODevice::ReadOnly ))
    {
        // Error while loading file
        qDebug()<< "Error while loading file";
        exit(1);
    }
     // Set data into the QDomDocument before processing
    xmlBOM.setContent(&f);
    f.close();

   // xmlBOM.setContent(szBufferXML);
    QString respuesta=this->leer_xml(xmlBOM);

    QList<QString>resultado=respuesta.split("//");

    if(resultado[0]=="0")
    {
        qDebug()<<"operacion denegada";
        this->imprimirTicketDenegada();
    }
    else
    {
        qDebug()<< "operacion correcta";
        this->imprimirTicketAnulacion();
    }
}

QString cobros::recarga(QString importe)
{
    QString operacion;

    short shStatus = -1;
    char szImporte[256];

    char szBufferXML[2048];

    QByteArray array = importe.toLocal8Bit();
    char* buffer = array.data();
    memcpy(szImporte,buffer,sizeof(szImporte));

    shStatus = fnDllOperPinPad(szImporte,"Ticket Factura","PAGO",szBufferXML,sizeof(szBufferXML));


    if(shStatus==0)
    {
        qDebug()<<"Operacion realizada correctamente. Hay que comprobar la transaccion";
        qDebug()<<szBufferXML;
        QFile f("pago.xml");

        if (!f.open(QIODevice::WriteOnly))
        {
            // Error while loading file
            qDebug()<< "Error while loading file";
            exit(1);
        }
        f.write(QByteArray(szBufferXML, sizeof(szBufferXML)));
        f.close();

        if (!f.open(QIODevice::ReadOnly ))
        {
            // Error while loading file
            qDebug()<< "Error while loading file";
            exit(1);
        }
         // Set data into the QDomDocument before processing
        xmlBOM.setContent(&f);
        f.close();

       // xmlBOM.setContent(szBufferXML);
        QString respuesta=leer_xml(xmlBOM);

        QList<QString>resultado=respuesta.split("//");
        if(resultado[0]=="0")
        {
            qDebug()<<"operacion denegada";
            operacion=QString::number(shStatus)+"//"+respuesta;
        }
        else
        {
            qDebug()<< "operacion correcta";
            operacion=QString::number(shStatus)+"//"+respuesta;
        }

    }
    else
    {
        qDebug()<<"Error: "+QString::number(shStatus);
        operacion=QString::number(shStatus)+"//";
    }



    return operacion;

}

QString cobros::anulacionRecarga(QString pedido, QString rts, QString importe)
{
    QString operacion;

    short shStatus = -1;
    char szBufferXML[2048];

    char szNumPedido[256];
    char szRTSOriginal[256];
    char szImporte[256];

    QByteArray array = importe.toLocal8Bit();
    char* buffer = array.data();
    memcpy(szImporte,buffer,sizeof(szImporte));


    QByteArray array2 = pedido.toLocal8Bit();
    char* buffer2 = array2.data();
    memcpy(szNumPedido,buffer2,sizeof(szNumPedido));

    QByteArray array3 = rts.toLocal8Bit();
    char* buffer3 = array3.data();
    memcpy(szRTSOriginal,buffer3,sizeof(szRTSOriginal));

    shStatus = fnDllOperComContable(szNumPedido,szRTSOriginal,szImporte,"Ticket Devolucion","DEVOLUCION",szBufferXML,sizeof(szBufferXML)-1);

    if(shStatus==0)
    {
        qDebug()<<"Operacion realizada correctamente. Hay que comprobar la transaccion";
        qDebug()<<szBufferXML;

        QFile f("devolucion.xml");

        if (!f.open(QIODevice::WriteOnly))
        {
            // Error while loading file
            qDebug()<< "Error while loading file";
            exit(1);
        }
        f.write(QByteArray(szBufferXML, sizeof(szBufferXML)));
        f.close();

        if (!f.open(QIODevice::ReadOnly ))
        {
            // Error while loading file
            qDebug()<< "Error while loading file";
            exit(1);
        }
         // Set data into the QDomDocument before processing
        xmlBOM.setContent(&f);
        f.close();

       // xmlBOM.setContent(szBufferXML);
        QString respuesta=this->leer_xml(xmlBOM);

        QList<QString>resultado=respuesta.split("//");

        if(resultado[0]=="0")
        {
            qDebug()<<"operacion denegada";
            operacion=QString::number(shStatus)+"//"+respuesta;
        }
        else
        {
            qDebug()<< "operacion correcta";
            operacion=QString::number(shStatus)+"//"+respuesta;
        }

    }
    else
    {
        qDebug()<<"Error: "+QString::number(shStatus);
        operacion=QString::number(shStatus)+"//";
    }

    return operacion;

}


void cobros::imprimir_recarga()
{
    QFile f("pago.xml");

    if (!f.open(QIODevice::ReadOnly ))
    {
        // Error while loading file
        qDebug()<< "Error while loading file";
        exit(1);
    }
     // Set data into the QDomDocument before processing
    xmlBOM.setContent(&f);
    f.close();

   // xmlBOM.setContent(szBufferXML);
    QString respuesta=this->leer_xml(xmlBOM);

    QList<QString>resultado=respuesta.split("//");

    if(resultado[0]=="0")
    {
        qDebug()<<"operacion denegada";
        this->imprimirTicketDenegada();
    }
    else
    {
        qDebug()<< "operacion correcta";
        this->imprimirTicket(respuesta);
    }
}


void cobros::imprimirTicketAnulacionRecarga()
{
    this->impresora->imprimirLogo("bicimad");

    QString texto="BiciMAD.\n";
    texto.append("Fecha: "+QDateTime::currentDateTime().toString("dd/MM/yyyy")+"   Hora: "+QTime::currentTime().toString("hh:mm:ss")+"\n\n");
    texto.append(QString::fromUtf8("No se ha podido recargar su saldo por motivos tecnicos.\nDisculpen las molestias.\n\n"));
    texto.append("             OPERACION ANULADA\n\n");

    this->impresora->imprimirTexto(texto);
}

